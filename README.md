# Install

## Commitlint

```bash
yarn add -D @commitlint/cli @commitlint/config-conventional
echo "module.exports = { extends: ['@commitlint/config-conventional'] };" > commitlint.config.js
```

# Husky

```bash
yarn add husky --D
yarn husky install
yarn husky add .husky/commit-msg 'yarn commitlint --edit $1'
```

# Commitizen prompt

```bash
 yarn add -D @commitlint/cz-commitlint commitizen
 yarn add @commitlint/config-conventional @commitlint/cli -D
```

Add to package.json

```json
{
  "scripts": {
    "commit": "git-cz"
  },
  "config": {
    "commitizen": {
      "path": "@commitlint/cz-commitlint"
    }
  }
}
```

## Try it

```bash
git commit -m "invalid commit message"
git commit -m "chore: valid commit message"
yarn commit
```

Reference

- https://www.npmjs.com/package/@commitlint/cz-commitlint
- https://commitlint.js.org/#/guides-local-setup

# Auto-Changelog

```bash
npm install -g auto-changelog
yarn auto-changelog -p
```

reference -https://www.npmjs.com/package/auto-changelog

# Generate-Changelog

```bash
yarn add generate-changelog -D
yarn changelog
```

reference

- https://www.npmjs.com/package/generate-changelog
